import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/resume-desktop.css';
import './assets/css/resume-small-desktop.css';
import './assets/css/resume-tablet.css';
import './assets/css/resume-phone.css';
import './assets/js/animations.js';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
