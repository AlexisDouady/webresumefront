import React, {Component} from 'react';

class Gauge extends Component
{

    render()
    {
        return(
            <div className="gauge-block">
                <strong className="gauge-name animated-child">{this.props.skill.name}</strong>
                <div className="gauge shadowed animated-child">
                    <div className="gauge-fill" style={{width: this.props.skill.value + "%"}}/>
                    <p className="gauge-percent">{this.props.skill.value} %</p></div>
            </div>
        );
    }

}

export default Gauge;