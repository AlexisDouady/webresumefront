import React, {Component} from 'react';

class Category extends Component
{
    render()
    {
        return(

            <section className={`l-section shadowed animated-hidden ${this.props.className !== undefined ? this.props.className : ''}`}>
                <h2 className="l-section-title animated-child"> { this.props.title } </h2>
                <article className="l-section-content">
                    { this.props.children }
                </article>
            </section>

        );
    }
}

export default Category;