import React, {Component} from 'react';
import Experience from './Experience.jsx';

class ExperienceList extends Component
{
    render()
    {
        return(
            <div className="experience-list">
                <div className="experience-list-experiences">
                    {this.props.experiences.map((experience, id) => (
                        <Experience experience={experience} key={id} icon={this.props.icons}/>
                    ))}
                </div>
                <div className="experience-list-bar animated-child">
                </div>
            </div>
        );
    }
}

export default ExperienceList;