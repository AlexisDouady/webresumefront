import React, {Component} from 'react';
import Background from '../assets/img/header_background.png';
import PresentationCard from "./PresentationCard";

class Header extends Component
{
    render()
    {
        return(

            <header className="l-header" style={{ backgroundImage: `url(${Background})` }}>
                <PresentationCard person={this.props.person}/>
            </header>

        );
    }
}

export default Header;