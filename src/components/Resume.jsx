import React, {Component} from 'react';
import Header from "./Header";
import Category from "./Category";
import portrait from "../assets/img/portrait-2.jpg";
import GaugeCategory from "./GaugeCategory";
import ExperienceList from './ExperienceList.jsx';


class Resume extends Component
{
    state = { resume: { data: { person: {}, skillCategories: [], schools: [], works: [] } } };

    constructor(props)
    {
        super(props);
        this.api = props.api;
        this.resumeId = props.resumeId;
    }

    getResume()
    {
        return this.state.resume.data;
    }

    getPerson()
    {
        return this.getResume().person;
    }

    getSkillCategories()
    {
        return this.getResume().skillCategories;
    }

    getSchools()
    {
        return this.getResume().schools;
    }

    getWorks()
    {
        return this.getResume().works;
    }

    componentDidMount()
    {
        fetch(
            'http://127.0.0.1:8080/api/resumes/' + this.resumeId,
            { method: 'GET', headers: new Headers(), mode: 'cors', cache: 'default' })
            .then(response => response.json())
            .then(resume => this.setState({resume: resume}));
    }

    render()
    {
        return (
            <main>
                <Header person={this.getPerson()}/>
                <Category title="Qui suis-je ?" className="l-bio">
                    <img className="l-bio-img shadowed animated-child" src={portrait} alt="Alexis DOUADY"/>
                    <p className="l-bio-p">
                        <i className="fa fa-quote-left animated-child"></i>
                        <br/>
                        <span className="l-bio-p-text animated-child">{this.getResume().description}</span>
                        <br/>
                        <i className="fa fa-quote-right animated-child"></i></p>
                </Category>
                <Category title="De quoi suis-je capable ?">
                    <div className="gauge-categories-list">
                        {this.getSkillCategories().map((category, id) => (
                            <GaugeCategory category={category} key={id}/>
                        ))}
                    </div>
                </Category>
                <Category title="Où se passent mes études ?">
                    <ExperienceList experiences={this.getSchools()} icons="fa fa-graduation-cap"/>
                </Category>
                <Category title="Où ai-je travaillé ?">
                    <ExperienceList experiences={this.getWorks()} icons="fa fa-briefcase"/>
                </Category>
            </main>
        );
    }
}

export default Resume;