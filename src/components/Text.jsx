import React, {Component} from 'react';

class Text extends Component
{

    render()
    {
        return (

            <p>
                { this.props.text.split('\\n').map((line, id) => {

                    return (<span key={id}>{line} <br/></span>)

                })}
            </p>
        );
    }

}

export default Text;