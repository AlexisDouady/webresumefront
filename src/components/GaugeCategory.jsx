import React, {Component} from 'react';
import Gauge from "./Gauge";

class GaugeCategory extends Component
{
    render()
    {
        return(

            <article className="gauge-category">
                <h3 className="title animated-child">{ this.props.category.title }</h3>
                {this.props.category.skills.map((skill, id) => (
                    <Gauge skill={skill} key={id}/>
                ))}
            </article>

        );
    }
}

export default GaugeCategory;