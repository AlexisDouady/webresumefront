import React, {Component} from 'react';
import portrait from '../assets/img/portrait.png';

class PresentationCard extends Component
{

    getPerson()
    {
        return this.props.person;
    }

    render()
    {
        return(

            <div className="l-card shadowed animated-hidden">
                <header>
                    <img className="l-card-image animated-child" src={portrait} alt="Alexis DOUADY"/>
                    <div className="l-card-title-block">
                        <h1 className="l-card-name animated-child">{this.getPerson().name + " " + this.getPerson().last_name}</h1>
                        <h3 className="l-card-status animated-child">{this.getPerson().status}</h3>
                    </div>
                </header>
                <table className="l-card-content">
                    <tbody>
                        <tr>
                            <td className={"animated-child"}>NÉ LE</td>
                            <td className={"animated-child"}>{this.getPerson().birth}</td>
                        </tr>
                        <tr>
                            <td className={"animated-child"}>ADRESSE</td>
                            <td className={"animated-child"}>{this.getPerson().city}</td>
                        </tr>
                        <tr>
                            <td className={"animated-child"}>E-MAIL</td>
                            <td className={"animated-child"}>{this.getPerson().email}</td>
                        </tr>
                        <tr>
                            <td className={"animated-child"}>TÉLÉPHONE</td>
                            <td className={"animated-child"}>{this.getPerson().phone}</td>
                        </tr>
                    </tbody>
                </table>
                <div className="networks-button animated-child">
                    <a href={this.getPerson().linkedin}><i className="fab fa-linkedin"></i></a>
                    <a href={this.getPerson().bitbucket}><i className="fab fa-git-alt"></i></a>
                </div>
            </div>

        );
    }
}

export default PresentationCard;