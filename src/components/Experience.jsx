import React, {Component} from 'react';
import Text from "./Text";

class Experience extends Component
{
    render()
    {
        return (

            <article className="experience shadowed animated-child">
                <i className={`experience-list-bar-item shadowed ${this.props.icon}`}/>
                <small className="experience-period animated-child">
                    {this.props.experience.beginning}
                    {this.props.experience.beginning === this.props.experience.end
                        ? ''
                        : '-' + this.props.experience.end}
                </small>
                <h3 className="title animated-child">{this.props.experience.title}</h3>
                <h3 className="subtitle animated-child">{this.props.experience.subtitle}</h3>
                <Text text={this.props.experience.description}/>
            </article>

        );
    }
}

export default Experience;