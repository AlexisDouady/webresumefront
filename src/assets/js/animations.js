const threshold = 0.1;
const animationsDuration = 800;
const options =
{
    root: null,
    rootMargin: '0px',
    threshold: threshold
};

function handleIntersect(entries, observer)
{
    entries.forEach(entry => {
        console.log(entry.intersectionRatio);
       if(entry.intersectionRatio > threshold)
       {
           let target = entry.target;
           target.classList.remove('animated-hidden');
           observer.unobserve(entry.target);

           let childList = target.querySelectorAll('.animated-child');
           childList.forEach((child, id) => {
               setTimeout(() => child.classList.remove('animated-child'),animationsDuration / childList.length * id)
           });
       }
    });
}

window.addEventListener('DOMContentLoaded', () => {
   const observer = new IntersectionObserver(handleIntersect, options);
   document.querySelectorAll('.animated-hidden').forEach(element => {observer.observe(element)});
});

